from django.db import models
# from django import forms
from django.forms import ModelForm


# Create your models here.
class User(models.Model):

	active = models.BooleanField(default=True)
	fullname = models.CharField(max_length=2000)
	email = models.CharField(max_length =200, unique=True)
	mobile = models.BigIntegerField(default =True, unique=True)
	address = models.TextField(editable = False)
	
	password = models.CharField(max_length =200, default ='')
	
	def __str__(self):
		return self.fullname+" - "+str(self.email)+"-"+str(self.id)