from django.http import Http404,HttpResponse
from django.shortcuts import get_object_or_404, render,redirect
from django.utils import timezone
from django import forms
from .models import User
import os
import json
from django.conf import settings
from django.http import JsonResponse

from rest_framework.response import Response

from django.views.decorators.csrf import csrf_protect
from django.template import loader
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view

from rest_framework.decorators import parser_classes
from rest_framework.parsers import JSONParser


def direct(request):
    context={}
    return render(request, 'direct.html', context)

@api_view(["POST", "GET"])
@parser_classes((JSONParser,))
def login(request, format=None):
    
    if request.method == "GET":
        template = loader.get_template("login.html")
        context = {}
        return HttpResponse(template.render(context, request))
    elif request.method == "POST":
        try:
            d=request.data
            print("d=",d)

            email=d['email']
            
            password=d['password']
            
            user = User.objects.get(email=email,password=password)
            
            return Response({"status": 200, "userId":user.id,"message": "Dear " + user.fullname + ", we successfully mailed your password"}, status=200)
        except :
            return Response({"status": 400, "message": "Email does not exist in our PMT account"}, status=200)


@api_view(["POST", "GET"])
@parser_classes((JSONParser,))    
def register(request, format=None):
    context={}
    if request.method=="POST":
        fullname=request.data['fullname']#parser classes using 
        email=request.data['email']
        mobile=request.data['mobile']
        address=request.data['address']
        password=request.data['password']

        user=User(fullname=fullname,address=address,email=email,mobile=mobile,password=password) 
        user.save()
        print("user saved")
        
        return Response({"status": 200, "message": "successfully registered", 'userId': user.id}, status=200)
 
    return render(request, 'register.html', context)    

def profile(request,id):
    context={}
    user = User.objects.get(pk=id)
    context['user']=user
    return render(request, 'profile.html', context)

@api_view(["POST", "GET"])
@parser_classes((JSONParser,))
def update(request,id,format=None):

    id=int(id)
    print('id',id)
    print('update...')
    print("meta data",request.META)
    print(request.path,request.method)
    if request.method == "GET":
        print("null..............",request.GET)
        user = User.objects.get(pk=id)
        print('getuser',user)
        context={'user':user}
        return render(request, 'update.html', context)

    elif request.method == "POST":
        print("null..............",request.POST)
        print("request data",request.data)
        fullname=request.data['fullname']#parser classes using 
        email=request.data['email']
        mobile=request.data['mobile']
        address=request.data['address']
        password=request.data['password']
       
        User.objects.filter(pk=id).update(fullname=fullname,address=address,email=email,mobile=mobile,password=password)

        return Response({"status": 200, "message": "successfully registered", 'userId': id}, status=200)
        # return redirect('/profile/'+id+'/')

# def delete(request,id):
#     context={}
#     id=int(id)
#     user = User.objects.get(pk=id)
#     user.active=False
#     user.save()
#     return redirect('/login/')

@api_view(["DELETE"])
def delete(request,id):
    id=int(id)
    user = User.objects.get(pk=id)
    user.delete()
    return redirect('/login/')