function profile(ev){
	ev.preventDefault()
	var data={
		email: document.forms["myform"]["email"].value,
		password:document.forms["myform"]["password"].value
	}
    //  "Content-Type": "application/json; charset=utf-8",
	console.log(data)
	// fetch('http://127.0.0.1:8000/login/', {
	fetch('/login/', {	
	        method: "POST", 
	        
	        headers: {
	            "Content-Type": "application/json; charset=utf-8",
	            // "Content-Type": "application/x-www-form-urlencoded" 
	        },
	        

	        body: JSON.stringify(data), // body data type must match "Content-Type" header
	    })
	    .then(response => response.json())
	    .then(response => {
	    	// console.log('response=')
	    	console.log(response)//{status: 200, userId: 27, message: "Dear gudal sharma, we successfully mailed your password"}

	    	if (response.status==200){//user found
	    		alert("user exists")
	    		//save user id in localstorage
	    		if (typeof(Storage) !== "undefined") {
				    // Store
				    localStorage.setItem("userId", response.userId);
				    console.log("userId successfully saved into localstorage..")
				    window.location.href = '/profile/'+response.userId;
				    return true;
				} else {
				    alert("Sorry, your browser does not support Web Storage...")
				}
	    	}else{
	    		alert("user does not exist")
	    		return false
	    	}
	    })
	    .catch(err => {
	    	console.log("error: " + err)
	    	return false
	    });
}
